package prefix_sum

type Summable interface {
	~int | ~int8 | ~int16 | ~int32 | ~int64 |
		~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~uintptr |
		~float32 | ~float64 | ~string
}

func Build[S ~[]T, T Summable](input S) S {
	if len(input) == 0 {
		return *new([]T) 
	}

	result := make([]T, len(input))

	result[0] = input[0]
	for i := 1; i < len(result); i++ {
		result[i] = result[i - 1] + input[i]
	}

	return result
}
