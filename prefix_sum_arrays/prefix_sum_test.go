package prefix_sum_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	prefix_sum "gitlab.com/telo_tade/prefix_suffix_arrays/prefix_sum_arrays"
)

func TestBuild(t *testing.T) {
	input := []int{14, -1, 1, -6, 1, 5, 12}
	expected := []int{14, 13, 14, 8, 9, 14, 26}

	got := prefix_sum.Build(input)

	assert.Equal(t, expected, got)
}

func FuzzBuild(f *testing.F) {
	f.Add([]byte{1, 2, 11, 12, 3, 3, 15, 8})

	f.Parallel()

	f.Fuzz(func(t *testing.T, slice []byte) {
		if len(slice) == 0 {
			assert.Equal(t, 0, len(prefix_sum.Build(slice)))

			return
		}

		got := prefix_sum.Build(slice)
		assert.Equal(t, got[0], slice[0])

		expected := slice[0]

		for i := 1; i < len(slice); i++ {
			expected += slice[i]

			assert.Equal(t, expected, got[i])
		}
	})
}
